#!/bin/sh
##
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)

cd "${PROGBASE}"
[ -f ./tools/chromedriver_linux64.zip ]
[ -f ./tools/chromedriver ] || { (cd tools && unzip chromedriver_linux64.zip;) }
[ -d site ] || mkdir site

LD_LIBRARY_PATH="${HOME}/.apt/usr/lib/x86_64-linux-gnu"
export LD_LIBRARY_PATH
python test.py >site/index.html

cd site
if git status --porcelain | grep -q M; then
	git add index.html
	git commit -m 'the site changed'
	git push
fi
