import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from BeautifulSoup import BeautifulSoup as bs

chrome_options = Options()
chrome_options.binary_location = '/home/grep/.apt/usr/bin/google-chrome'
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--headless')
driver = webdriver.Chrome(executable_path='./tools/chromedriver', chrome_options=chrome_options)


driver.get('https://2019.csgames.org')
sp = bs(driver.page_source)
print(sp.prettify())
driver.quit()
